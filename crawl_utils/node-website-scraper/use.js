import scrape from 'website-scraper'; // only as ESM, no CommonJS
const options = {
  urls: ['http://guanjia.qq.com/act/sem/202007qqmusic/'],
  directory: './1/'
};
try {
// with async/await
  const result = await scrape(options);

// with promise
  scrape(options).then((result) => {});
} catch(e) {
  console.log('failed');
}
