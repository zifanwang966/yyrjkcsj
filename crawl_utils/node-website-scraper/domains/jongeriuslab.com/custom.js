jQuery(document).ready(function($) { // begin global

/*-----------------------------------------------------------------------------------*/
/* Main navigation - push down
/*-----------------------------------------------------------------------------------*/

	$('.menu-text').hide();
		$('.menu-toggle').click(function () {
		$('.menu-text').slideToggle('100');
		$('.menu-toggle').toggleClass('active');
	});

/*-----------------------------------------------------------------------------------*/
/* Nivoslider
/*-----------------------------------------------------------------------------------*/

	$(window).load(function() {
	    $('#slider').nivoSlider({
	    effect:'fade'
	    });
	});
	
/*-----------------------------------------------------------------------------------*/
/* Masonry
/*-----------------------------------------------------------------------------------*/
/*
  $('#home #content').masonry({
      itemSelector : '',
      columnWidth : 160,
      isAnimated: true,
      isOriginLeft: true
  });
*/
/*-----------------------------------------------------------------------------------*/
/* Flexslider
/*-----------------------------------------------------------------------------------*/
	
	$(window).load(function() {
        $('.flexslider').flexslider({
          animation: "fade",
          controlsContainer: ".flex-container",
          start: function(slider) {
            $('.total-slides').text(slider.count);
          },
          after: function(slider) {
            $('.current-slide').text(slider.currentSlide);
          }
       });
     });

});// end global


