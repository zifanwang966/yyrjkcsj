import os
import multiprocessing as mp
from re import sub
from shutil import copy
import subprocess
import time

BASEDIR = '/Users/wangzifan/Desktop/dachuang/wasm-collector'
NUMOFPROCESS = 20
TARGETLINE = 22


def copy_mainjs():
    mainjs = os.path.join(BASEDIR, 'main.js')
    for i in range(NUMOFPROCESS):
        mainjs_copy = mainjs.replace('main', 'main-%d'%i)
        if not os.path.exists(mainjs_copy):
            copy(mainjs, mainjs_copy)


def replaceurl(mainjs, url):
    f = open(mainjs, 'r')
    lines = f.readlines()
    line = '    await page.goto("http://%s");\n'%url
    lines[TARGETLINE] = line
    text = ''.join(lines)
    f.close()
    f = open(mainjs, 'w')
    f.write(text)
    f.close()


def crawl_wasm(domains, process_no, checked_domain, failed_domain, wasm_domain):
    mainjs = os.path.join(BASEDIR, 'main-%d.js'%process_no)
    for domain in domains:
        replaceurl(mainjs, domain)
        f = open(mainjs)
        line = f.readlines()[TARGETLINE]
        # print(line)
        checked_domain.append(line)
        # (status_before, output_before) = subprocess.getstatusoutput('ls -l /home/ubuntu/wasm-collector/wasm | grep "^-" | wc -l')
        # (status, output) = subprocess.getstatusoutput('node main-%d.js'%process_no)
        # (status_after, output_after) = subprocess.getstatusoutput('ls -l /home/ubuntu/wasm-collector/wasm | grep "^-" | wc -l')
        # if int(output_after) > int(output_before):
        #     wasm_domain.append(output_after+','+domain)
        # if output == '':
        #     print(domain, ' checked.')
        #     checked_domin.append(domain)
        # else:
        #     print(domain, 'failed.')
        #     failed_domain.append(domain)
        #     continue



def test_replaceurl():
    mainjs = '/Users/wangzifan/Desktop/dachuang/wasm-collector/main.js'
    url = 'www.baidu.com'
    for i in range(2):
        replaceurl(mainjs, url)


def test_single_process(lines):
    domains = [line.split(',')[1].replace('\n', '') for line in lines]
    checked_domin = []
    failed_domain = []
    wasm_domain = []
    crawl_wasm(domains,0,checked_domin, failed_domain, wasm_domain)

if __name__ == '__main__':
    checked_domin = mp.Manager().list()
    failed_domain = mp.Manager().list()
    wasm_domain = mp.Manager().list()
    copy_mainjs()
    f = open('/Users/wangzifan/Desktop/dachuang/top-1m.csv')
    lines = f.readlines()
    lines_per_process = int(len(lines) / NUMOFPROCESS)
    pointer = 0
    # pool = mp.Pool(NUMOFPROCESS)
    
    for i in range(NUMOFPROCESS):
        if i == NUMOFPROCESS - 1:
            lines_block = lines[pointer:]
        else:
            lines_block = lines[pointer:pointer+lines_per_process]
            pointer += lines_per_process
        domains = [line.split(',')[1].replace('\n', '') for line in lines_block]
        # pool.apply_async(crawl_wasm, args=(domains,i,checked_domin, failed_domain, wasm_domain,))
        process = mp.Process(target=crawl_wasm, args=(domains,i,checked_domin, failed_domain, wasm_domain))
        process.start()
    
    # pool.join()


    while(len(checked_domin)+len(failed_domain) != 1000000):
        time.sleep(10)

    with open('./checked_domain.txt', 'w') as f:
        f.write('\n'.join(checked_domin))
    with open('./failed_domain', 'w') as f:
        f.write('\n'.join(failed_domain))
    with open('./wasm_domain', 'w') as f:
        f.write('\n'.join(wasm_domain))
    
    
    # test_replaceurl()
    # test_single_process(lines)


