<<<<<<< HEAD
import os
import re
import subprocess
from main.mylib.csv2grey import convert_csv_to_png

TMPDIR = 'F:/sjtu/web_show/mysite/'


def dump_wasm(wasm_path):
    # 建立用于临时存放反汇编文件、提取出的十六进制csv文件
    if not os.path.exists(TMPDIR):
        os.makedirs(TMPDIR)
    #print(wasm_path)
    wasm_stm = wasm_path
    wasm_name = wasm_path.split('/')[-1].replace('.wasm', '')
    print(wasm_stm)
    dump_file_path = os.path.join(TMPDIR, wasm_name + '.txt')
    ret = subprocess.getoutput('wasm-objdump.exe' + ' -d ' + wasm_stm + ' > ' + dump_file_path)
    #print(dump_file_path)
    #subprocess.run(['wasm-objdump', '-d', wasm_path, '>', dump_file_path])
    return dump_file_path


def hex2bi(hexstring):
    hexlist = []
    for ch in hexstring:
        if ch != ' ':
            hexlist.append(ch)
        # ch = ord(ch)
        # if ch >= ord('0') and ch <= ord('9'):
        #     hexlist.append(ch-ord('0'))
        # elif ch >= ord('a') and ch <= ord('f'):
        #     hexlist.append(ch-ord('a')+10)
        # else:
        #     continue
    bilist = []
    for num in hexlist:
        bistring = bin(int(num, 16))[2:]
        bilist.extend(['0'] * (4 - len(bistring)))
        for bi in bistring:
            bilist.append(bi)

    while len(bilist) < 64:
        bilist.append('0')
    bilist = bilist[0:64]

    return bilist


def match_line(line, graph):
    hexstring = re.findall(":(.*?)[|]", line)
    if len(hexstring) == 0:
        return
    hexstring = hexstring[0]
    if hexstring == '':
        return
    hexstring = hexstring.strip()
    bilist = hex2bi(hexstring)
    graph.append(bilist)


def save_graph_as_csv(graph, dump_file_path):

    csv_file_path = dump_file_path.replace('txt', 'csv')
    f = open(csv_file_path, 'a')
    for line in graph:
        string = ','.join(line)
        f.write(string + '\n')
    f.close()
    os.remove(dump_file_path)
    return csv_file_path
    # print(csvname + ' saved.')


def wasm2png(wasm_path, png_dir):
    dump_file_path = dump_wasm(wasm_path)
    graph = []
    with open(dump_file_path) as f:
        lines = f.readlines()
        for line in lines:
            match_line(line, graph)

    csv_file_path = save_graph_as_csv(graph, dump_file_path)
    return convert_csv_to_png(csv_file_path, png_dir)

=======
import os
import re
import subprocess
from mylib.csv2grey import convert_csv_to_png


TMPDIR = ''

def dump_wasm(wasm_path):
    #建立用于临时存放反汇编文件、提取出的十六进制csv文件
    if not os.path.exists(TMPDIR):
        os.makedirs(TMPDIR)
    wasm_name = wasm_path.splits('/')[-1]
    dump_file_path = os.path.join(TMPDIR, wasm_name+'.txt')
    subprocess.run(['wasm-objdump', '-d', wasm_path, '>', dump_file_path])
    return dump_file_path



def hex2bi(hexstring):
    hexlist = []
    for ch in hexstring:
        if ch != ' ':
            hexlist.append(ch)
        # ch = ord(ch)
        # if ch >= ord('0') and ch <= ord('9'):
        #     hexlist.append(ch-ord('0'))
        # elif ch >= ord('a') and ch <= ord('f'):
        #     hexlist.append(ch-ord('a')+10)
        # else:
        #     continue
    bilist = []
    for num in hexlist:
        bistring = bin(int(num, 16))[2:]
        bilist.extend(['0'] * (4 - len(bistring)))
        for bi in bistring:
            bilist.append(bi)

    while len(bilist) < 64:
        bilist.append('0')
    bilist = bilist[0:64]
    
    return bilist
    

def match_line(line, graph):
    hexstring = re.findall(":(.*?)[|]", line)
    if len(hexstring) == 0:
        return
    hexstring = hexstring[0]
    if hexstring == '':
        return
    hexstring = hexstring.strip()
    bilist = hex2bi(hexstring)
    graph.append(bilist)


def save_graph_as_csv(graph, dump_file_path):
    csv_file_path = dump_file_path.replace('txt', 'csv')
    f = open(csv_file_path, 'a')
    for line in graph:
        string = ','.join(line)
        f.write(string+'\n')
    f.close()
    os.remove(dump_file_path)
    return csv_file_path
    # print(csvname + ' saved.')

    

def wasm2png(wasm_path, png_dir):
    dump_file_path = dump_wasm(wasm_path)
    graph = []
    with open(dump_file_path) as f:
        lines = f.readlines()
        for line in lines:
            match_line(line, graph)
    
    
    csv_file_path = save_graph_as_csv(graph, dump_file_path)
    convert_csv_to_png(csv_file_path, png_dir)

>>>>>>> 094f8b7ee233ebbd6adf391894bd713a7db5420a
