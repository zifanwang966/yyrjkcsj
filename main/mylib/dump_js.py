import os
import subprocess
from mylib.csv2grey import convert_csv_to_png

TMPDIR = ''



def hex2bi(hexstring):
    hexlist = []
    for ch in hexstring:
        if ch != ' ':
            hexlist.append(ch)
        # ch = ord(ch)
        # if ch >= ord('0') and ch <= ord('9'):
        #     hexlist.append(ch-ord('0'))
        # elif ch >= ord('a') and ch <= ord('f'):
        #     hexlist.append(ch-ord('a')+10)
        # else:
        #     continue
    bilist = []
    for num in hexlist:
        bistring = bin(int(num, 16))[2:]
        bilist.extend(['0'] * (4 - len(bistring)))
        for bi in bistring:
            bilist.append(bi)

    while len(bilist) < 64:
        bilist.append('0')
    bilist = bilist[0:64]
    
    return bilist
    



def match_line(line, graph):
    if len(line) < 32:
        return
    if line[31] != ':':
        return
    else:
        hexstring = line[33:51]
    hexstring = hexstring.strip()
    bilist = hex2bi(hexstring)
    graph.append(bilist)


def save_graph_as_csv(graph, dump_file_path):
    csv_file_path = dump_file_path.replace('.txt', '.csv')

    f = open(csv_file_path, 'a')
    for line in graph:
        string = ','.join(line)
        f.write(string+'\n')
    f.close()
    os.remove(dump_file_path)
    return csv_file_path

def dump_file(js_file_path):
    js_file_name = js_file_path.splits('/')[-1]
    dump_file_path = os.path.join(TMPDIR, js_file_name+'.txt')
    subprocess.run(['node', '--print-bytecode', js_file_path, '>', dump_file_path])
    return dump_file_path

def dump_js(js_file_path, png_dir):
    dump_file_path = dump_file(js_file_path)

    graph = []
    with open(js_file_path) as f:
        lines = f.readlines()
        for line in lines:
            match_line(line, graph)
            if len(graph) >= 6400:
                break
    csv_file_path = save_graph_as_csv(graph, dump_file_path)
    convert_csv_to_png(csv_file_path, png_dir)