import os
import subprocess

BASEDIR = ''
TARGETLINE = 23


def replace_domain(domain):
    def replaceurl(mainjs, domain):
    f = open(mainjs, 'r')
    lines = f.readlines()
    line = '    await page.goto("http://%s");\n'%domain
    lines[TARGETLINE] = line
    text = ''.join(lines)
    f.close()
    f = open(mainjs, 'w')
    f.write(text)
    f.close()


def crawl_wasm(domain):
    mainjs = os.path.join(BASEDIR, 'main.js')
    replace_domain(mainjs, domain)
    check_file_num = 'ls -l %s | grep "^-" | wc -l'%(os.path.join(BASEDIR, 'wasm-collector/wasm/'))
    (status_before, output_before) = subprocess.getstatusoutput(check_file_num)
    #(status, output) = subprocess.getstatusoutput('node main-%d.js'%process_no)
    print('checking %s for WebAssembly file ...'%domain)
    os.system('node main.js')
    (status_after, output_after) = subprocess.getstatusoutput(check_file_num)
    if int(output_after) > int(output_before):
        return 1
    else:
        return 0    