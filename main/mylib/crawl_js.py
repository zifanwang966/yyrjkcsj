import os
import shutil
from shutil import copy
from shutil import move
from shutil import rmtree
import subprocess



NUMOFPROCESS = 5
TMPDIR = '' #用于存放网站得到的全部文件
USEJS = ''




def replace_domain(domain):
    domain_dir = os.path.join(TMPDIR, domain)
    usejs_path = ''
    f = open(USEJS)
    lines = f.readlines()
    f.close()
    lines[2] = "  urls: ['http://%s'],\n"%domain
    lines[3] = "  directory: '%s'\n"%domain_dir
    text = ''.join(lines)
    f = open(USEJS, 'w')
    f.write(text)
    f.close()



def crawl_js(domain):
    domain_dir = os.path.join(TMPDIR, domain)
    replace_domain(domain)
    subprocess.run(['node', USEJS])
    if not os.path.exists(domain_dir):
        return
    for root, dirs, files in os.walk(domain_dir):
        for dir in dirs:
            if dir != 'js':
                rmtree(os.path.join(root, dir))
        for file in files:
            if not file.endswith('.js'):
                os.remove(os.path.join(root, file))
    js_dir = os.path.join(domain_dir, 'js')
    if not os.path.exists(js_dir):
        rmtree(domain_dir)
        return
    for root, dirs, files in os.walk(js_dir):
        for file in files:
            js_path = os.path.join(root, file)
            move(js_path, domain_dir)
    rmtree(js_dir)


