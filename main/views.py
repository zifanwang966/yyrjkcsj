<<<<<<< HEAD
from django.shortcuts import render
from django.http import HttpResponse
import os
from django.views.decorators.csrf import csrf_exempt
import numpy as np
import os
import cv2
from tensorflow import keras
import tensorflow as tf
from django.contrib.auth.decorators import login_required
from main.mylib.dump_wasm import wasm2png
from django.http import JsonResponse
import hashlib
import time
import shutil
import subprocess


# 绝对路径（测试时请修改此处）
# 由于Django改路径有点难顶，所以采用绝对路径。。
# 同时也更改mylib/dump_wasm.py中的TMPDIR

ab_path = "F:/sjtu/web_show/mysite/"


def CalcFileSha256(filname):
    """ 计算文件 sha256 """

    with open(filname, "rb") as f:
        sha256obj = hashlib.sha256()
        sha256obj.update(f.read())
        hash_value = sha256obj.hexdigest()
        return hash_value


def binary2img(src_file_path, imgdir):
    """ 二进制wasm文件转为图片，并检测 """

    wasmdir = os.path.join(imgdir, 'png_from_wasm')
    if not os.path.exists(wasmdir):
        os.makedirs(wasmdir)
    result = wasm2png(src_file_path, wasmdir)

    return result

def check(file_name, user_id):
    # 0-miner 1-n_miner

    # model init
    net = keras.models.load_model('wasm_c.h5')

    # img to test
    tar_dir = '../file_upload/' + str(user_id) + '/'
    tar = tar_dir + str(file_name)  # path image_saved
    img = cv2.imread(tar, cv2.IMREAD_GRAYSCALE)
    img.resize(1, 6400, 64, 1)

    prediction = tf.nn.softmax(net.predict(img)[0])
    score = 100 * np.max(prediction)
    label_pre = np.argmax(prediction)

    return label_pre, score


# Create your views here.
@login_required(login_url='/login/')
def index(request):
    """ 主页面 """
    return render(request, 'index.html')


def result(request):
    """ 结果展示 """

    context = {}
    user_id = request.user.id
    file_path = find_new_file(user_id)

    #传给前端的内容
    context['file_name'] = file_path.split('/')[-1]
    context['sha256'] = CalcFileSha256(file_path)
    context['create_time'] = time.ctime(os.path.getmtime(file_path))

    result = binary2img(file_path, 'img_saved')[0]
    if result == 0:
        context['result'] = '安全'
    else:
        context['result'] = '恶意'

    img_path = binary2img(file_path, 'img_saved')[1]
    img_path = ab_path + img_path
    shutil.copy(img_path, ab_path + 'static/images')
    context['img_path'] = img_path.split('\\')[-1]

    return render(request, 'result.html', context)


@csrf_exempt
def upload(request):
    """ 接受前端上传的文件 """

    if request.method == 'POST':
        file = request.FILES.get('file')
        user_id = request.user.id
        user_dir = 'file_upload/' + str(user_id)
        os.makedirs(user_dir, exist_ok=True)
        # 写入后台
        with open(user_dir + '/' + file.name, 'wb+') as f:
            for chunk in file.chunks():
                f.write(chunk)
            f.close()
        # file_path = find_new_file(user_id)
        return JsonResponse({"status": "success", "filename": file.name})
        # return render(request, 'result.html', {'name': file.name})


def find_new_file(user_id):
    """ return user_id newest upload_file path """

    dir = ab_path + 'file_upload/' + str(user_id)
    file_lists = os.listdir(dir)
    file_lists.sort(key=lambda fn: os.path.getmtime(dir + "\\" + fn)
    if not os.path.isdir(dir + "\\" + fn) else 0)

    file = dir + '/' + file_lists[-1]

    return file

# if __name__ == '__main__':
#     a = find_new_file(8)
#
#     # wasm_path = 'F:/sjtu/web_show/mysite/file_upload/3' \
#     #             '/0af18a204e2b8823959da589178f5b72dfa6ac3e816890b11323bb31833007fc.wasm '
#     # dump_file_path = 'F:/sjtu/web_show/mysite/a.txt'
#     # #open(dump_file_path, 'a').close()
#     #
#     # #os.popen('./mylib/wasm-objdump.exe -d ' + wasm_path + ' > ' + dump_file_path)
#     # ret = subprocess.getoutput('wasm-objdump.exe' + ' -d ' + wasm_path + ' > ' + dump_file_path)
#     # #print(ret)
#     # print("a")
#     print(binary2img(a, 'img_saved'))
=======
from django.shortcuts import render
from django.http import HttpResponse
import os
from django.views.decorators.csrf import csrf_exempt
import numpy as np
import os
import cv2
from tensorflow import keras
import tensorflow as tf
from mylib.crawl_wasm import crawl_wasm
from mylib.dump_wasm import wasm2png
from mylib.crawl_js import crawl_js
from mylib.dum_js import js2png



# Process function

def binary2img(src_file_path, imgdir):
    # 二进制的包含路径的文件名，以及存放img的文件夹完整路径
    wasmdir = os.path.join(imgdir, 'png_from_wasm')
    jsdir = os.path.join(imgdir, 'png_from_js')
    if not os.path.exists(wasmdir):
        os.makedirs(wasmdir)
    if not os.path.exists(jsdir):
        os.makedirs(jsdir)
    wasm2png(src_file_path, wasmdir)
    js2png(src_file_path, jsdir)


def url_crawl(url):
    # return_code 1 为找到，0为没找到
    wasm_return_code = crawl_wasm(url)
    js_return_code = crawl_js(url) # 还没写


def check(file_name, user_id):
    # 0-miner 1-n_miner

    # model init
    net = keras.models.load_model('wasm_c.h5')

    # img to test
    tar_dir = '../file_upload/' + str(user_id) + '/'
    tar = tar_dir + str(file_name)  # path image_saved
    img = cv2.imread(tar, cv2.IMREAD_GRAYSCALE)
    img.resize(1, 6400, 64, 1)

    prediction = tf.nn.softmax(net.predict(img)[0])
    score = 100 * np.max(prediction)
    label_pre = np.argmax(prediction)

    return label_pre, score


# Create your views here.
def index(request):
    return render(request, 'index.html')


@csrf_exempt
def upload(request):
    if request.method == 'POST':
        file = request.FILES.get('fafafa')
        user_id = request.user.id
        user_dir = 'file_upload/' + str(user_id)
        os.makedirs(user_dir, exist_ok=True)
        with open(user_dir + '/' + file.name, 'wb+') as f:
            for chunk in file.chunks():
                f.write(chunk)
            f.close()

        return render(request, 'result.html', {'name': file.name})

# if __name__ == '__main__':
#     a = '20200811234837_1.jpg'
#     check(a, 2)
>>>>>>> 094f8b7ee233ebbd6adf391894bd713a7db5420a
