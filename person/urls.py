from django.urls import path
from . import views

urlpatterns = [
    path('person/', views.person_page, name='person'),
]