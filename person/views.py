from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from person.models import MyUser
from django.http import HttpResponse
import os


def person_page(request):
    context = {}
    user_id = request.user.id

    context['username'] = MyUser.objects.get(id=user_id)
    context['email'] = MyUser.objects.get(id=user_id).email
    user_upload_dir = 'file_upload/' + str(user_id)
    for _, _, files in os.walk(user_upload_dir):
        for f in files:
            context.setdefault('files', []).append(f)

    return render(request, 'person.html', context)
