from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt


# Create your views here.

@csrf_exempt
def get_login(request):
    if request.user.is_authenticated:
        return redirect('index')        # index是主页
    else:
        if request.method == "POST":
            username = request.POST.get('user')
            password = request.POST.get('pass')
            #email = request.POST.get('email')
            #print(email)
            auth = authenticate(request, password=password, username=username)
            if auth is not None:
                #print('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                login(request, auth)
                messages.add_message(request, messages.INFO, 'Login successfully complete')
                return redirect('index')
            #print('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb')

    return render(request, "login.html")



def get_logout(request):
    logout(request)
    return redirect('index')