from django import forms
from person.models import MyUser


class SignupModelForm(forms.ModelForm):
    class Meta:
        model = MyUser

        fields = [
            'username',
            'email',
            'password'
        ]
